# Codebase release 1.0 for FermiFCI

by Lukas Rammelmüller, David Huber, Artem G. Volosniev

SciPost Phys. Codebases 12-r1.0 (2023) - published 2023-04-19

[DOI:10.21468/SciPostPhysCodeb.12-r1.0](https://doi.org/10.21468/SciPostPhysCodeb.12-r1.0)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.12-r1.0) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Lukas Rammelmüller, David Huber, Artem G. Volosniev

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Live (external) repository at [https://github.com/rammelmueller/FermiFCI.jl/tree/3a4bc10261aa112194195c82333a9f6ed312c004](https://github.com/rammelmueller/FermiFCI.jl/tree/3a4bc10261aa112194195c82333a9f6ed312c004)
* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.12-r1.0](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.12-r1.0)
